package com.lukianchenko.task2_SerializationAndDeserialization;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class SaverLoader {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Logger logger = LogManager.getLogger(SaverLoader.class);
        SomeData someData = new SomeData();
        someData.setGreen(true);
        someData.setLength(57);
        someData.setName("Bobbigale");
        someData.setYear(1645);

        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("someData.txt"));
        out.writeObject(someData);
        out.close();

        ObjectInputStream in = new ObjectInputStream(new FileInputStream("someData.txt"));
        SomeData someData2 = (SomeData)in.readObject();
        in.close();
        logger.info("name = " + someData2.getName());
        logger.info("year = " + someData2.getYear());

    }
}
