package com.lukianchenko.task2_SerializationAndDeserialization;

import java.io.Serializable;

public class SomeData implements Serializable {
    private int year;
    private String name;
    private int length;
    private transient boolean isGreen;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isGreen() {
        return isGreen;
    }

    public void setGreen(boolean green) {
        isGreen = green;
    }

    public void sayHello(){
        System.out.println("Hello");
    }
}
